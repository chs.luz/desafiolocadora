Navegar atá a raiz do projeto utilizando o terminal
e rodar o comando

### `npm intall`

alterar a KEY para acesso a API de teste dentro de src/services/api.js

### importante: O projeto não utiliza nenhum banco de dados, então está sendo utilizado o redux para simular um banco
### o login é apenas uma demonstração então qualquer login e senha servem para teste

Para rodar o projeto utilizar o comando:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Para rodar os testes
### `npm test`