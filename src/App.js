import React from 'react';
import GlobalStyle from './styles';
import Routes from './routes';

function App() {
  return (
    <>
        <Routes />
        <GlobalStyle />
    </>
  );
}

export default App;
