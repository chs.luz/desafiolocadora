import axios from 'axios';

const key = '';

const api = axios.create({
    baseURL: `http://www.omdbapi.com/`
})

export const findTitle = `?apikey=${key}&t=`;
export const findById = `?apikey=${key}&i=`;

export default api;