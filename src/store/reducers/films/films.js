import { SEARCHED_FILM, ADD_FAVORITE_MOVIE, REMOVE_FAVORITE_MOVIE,CLEAR_DATA } from '../../const';
const INITIAL_STATE = {
    searchedFilm: {},
    favoriteMovies: []
}

export default function (state=INITIAL_STATE,action) {
    switch(action.type) {
        case SEARCHED_FILM: {
            return {...state,searchedFilm: action.film}
        }
        case ADD_FAVORITE_MOVIE: {
            let movies = [...state.favoriteMovies];
            if(action.film.imdbID) {
                movies.push(action.film);
            }
            return {...state,favoriteMovies: movies};
        }
        case REMOVE_FAVORITE_MOVIE: {
            let movies = state.favoriteMovies.filter(f => {
                return f.imdbID !== action.film.imdbID
            })
            return {...state,favoriteMovies: movies};
        }
        case CLEAR_DATA: return INITIAL_STATE;
        default: return state;
    }
}