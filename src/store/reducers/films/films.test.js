import { SEARCHED_FILM, ADD_FAVORITE_MOVIE, REMOVE_FAVORITE_MOVIE, CLEAR_DATA } from '../../const';
import home from './films';

const INITIAL_STATE = {
    searchedFilm: {},
    favoriteMovies: []
}

it('SHOULD RETURN EMPTY FILM WITHOUT SEND FILM', () => {
    expect(home(undefined,{type:"UNKNOWN"})).toEqual(INITIAL_STATE);
})

it('SHOULD RETURN EMPTY FILM', () => {
    let searchedFilm = {};
    expect(home(undefined,{type:SEARCHED_FILM,film:searchedFilm})).toEqual(INITIAL_STATE);
});

it('SHOULD RETURN FILM OBJECT', () => {
    let state = {...INITIAL_STATE};
    state.searchedFilm = {Title: 'New Film'};
    expect(home(undefined, {type:SEARCHED_FILM,film: state.searchedFilm})).toEqual(state);
})

it('SHOULD RETURN INITIAL STATE WHEN ADD FAVORITE MOVE EMPTY', () => {
    expect(home(INITIAL_STATE,{ type: ADD_FAVORITE_MOVIE,film: {}})).toEqual(INITIAL_STATE);
})

it('SHOULD RETURN MY FAVORITES WITH ONE FAVORITE MOVIE', () => {
    let state = {...INITIAL_STATE};
    state.favoriteMovies = [];
    state.favoriteMovies.push({imdbID: '1',Title: 'New Film'});
    expect(home(INITIAL_STATE,{ type: ADD_FAVORITE_MOVIE,film: state.favoriteMovies[0]})).toEqual(state);
})

it('SHOULD RETURN INITIAL STATE WHEN REMOVE FAVORITE MOVE EMPTY', () => {
    expect(home(INITIAL_STATE,{ type: REMOVE_FAVORITE_MOVIE,film: {}})).toEqual(INITIAL_STATE);
})

it('SHOULD RETURN MY FAVORITES WITH NO ONE FAVORITE MOVIE', () => {
    let state = {...INITIAL_STATE};
    state.favoriteMovies = [];
    state.favoriteMovies.push({imdbID: '1',Title: 'New Film'});
    expect(home(INITIAL_STATE,{ type: REMOVE_FAVORITE_MOVIE,film: state.favoriteMovies[0]})).toEqual(INITIAL_STATE);
})


it('SHOULD RETURN CLEAR DATA', () => {
    let state = {...INITIAL_STATE};
    state.favoriteMovies = [];
    state.favoriteMovies.push({imdbID: '1',Title: 'New Film'});
    expect(home(state,{ type: CLEAR_DATA})).toEqual(INITIAL_STATE);
})

