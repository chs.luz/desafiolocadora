import { SEARCHED_FILM, ADD_FAVORITE_MOVIE, REMOVE_FAVORITE_MOVIE, CLEAR_DATA} from '../const';

export const searchedFilm = (film) => {
    return {
        type: SEARCHED_FILM,
        film
    }
}

export const addNewFavorite = (film) => {
    return {
        type: ADD_FAVORITE_MOVIE,
        film
    }
}

export const removeFavoriteMovie = (film) => {
    return {
        type: REMOVE_FAVORITE_MOVIE,
        film
    }
}

export const clearData = () => {
    return {
        type: CLEAR_DATA
    }
}