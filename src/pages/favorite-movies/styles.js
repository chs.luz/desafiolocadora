import styled from 'styled-components';
import { MdRemove } from 'react-icons/md'

export const Container = styled.div `
    width: 100%;
    height: 100%;
    margin: 0 auto;
    padding: 20px 20px;
    text-align: center;
    overflow:auto;
`
export const UL = styled.ul `
    list-style: none;
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 30px;
    margin-top: 30px;
`

export const Card = styled.div `
    display: flex;
    flex-direction:column;
    width: 250px;
    height: 300px;
`


export const CardTitle = styled.div `
    display: flex;
    flex-direction:row;
    height: 30px;
`

export const Title = styled.label `
    display: flex;
    flex: 4;
    font-size: 16px;
    font-weight: bold;
`

export const Favorite = styled.div `
    display: flex;
    flex: 1;
    align-items:flex-end;
    justify-content:center;
`

export const Imagem = styled.img `
    width: 95%;
    max-height: 280px;
    cursor:pointer;
`

export const NoFavoriteIcon = styled(MdRemove) `
    margin: 10px;
    :hover {
        color: gray;
        cursor:pointer;
    }
`

export const ContainerNoContent = styled.div `
    display:flex;
    flex-direction: column;
    align-items:center;
    justify-content:center;
    width: 100%;
    height: 200px;
`