import React from 'react';
import {
    Container,
    UL,
    Card,
    CardTitle,
    Title,
    Favorite,
    NoFavoriteIcon,
    Imagem,
    ContainerNoContent
} from './styles';
import { withRouter } from 'react-router-dom';
import Template from '../../components/Template';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { removeFavoriteMovie } from '../../store/actions/films';

function FavoriteMovies({history, removeFavoriteMovie, favoriteMovies }) {

    const sendToDetail = (film) => {
        history.push(`/film-detail/${film.imdbID}`)
    } 

    return (
        <Template>
            <Container>
                {
                    favoriteMovies.length > 0 ?
                        (
                            <UL>
                                {
                                    favoriteMovies.map(film => (
                                        <Card key={film.imdbID}>
                                            <CardTitle>
                                                <Title>
                                                    {film.Title}
                                                </Title>
                                                <Favorite>
                                                    <NoFavoriteIcon
                                                        onClick={() => removeFavoriteMovie(film)}
                                                        size={30} />
                                                </Favorite>
                                            </CardTitle>
                                            <Imagem onClick={() => sendToDetail(film)} src={film.Poster} />
                                        </Card>
                                    ))
                                }
                            </UL>
                        )
                        :
                        <ContainerNoContent>
                            <h1>Você ainda não tem nenhum filme nos favoritos :(</h1>
                        </ContainerNoContent>
                }

            </Container>
        </Template>
    )
}

const mapStateToProps = state => {
    return {
        favoriteMovies: state.films.favoriteMovies
    };
};

const mapDispatchToProps = dispath => {
    return bindActionCreators({ removeFavoriteMovie }, dispath);
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(FavoriteMovies));