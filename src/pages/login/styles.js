import styled from 'styled-components';

export const Container = styled.div `
    display:flex;
    flex-direction: column;
    width:100%;
    height:100%;
    align-items:center;
    background:#f0f2f5;
    justify-content: center;
`


export const ContainerLogin = styled.div`
    display: flex;
    flex-direction:column;
    width: 400px;
    height:300px;
    border-style: solid;
    border-width: 1px;
    border-radius: 5px;
`

export const Label = styled.label `
    padding: 10px;
    font-size: 18px;
    font-weight: bold;
`

export const Input = styled.input `
    margin: 5px 10px;
    padding-left: 15px;
    width: 95%;
    height: 40px;
    color: gray;
`

export const Button = styled.button `
    margin: 5px 10px;
    width: 95%;
    height: 40px;
    color: gray;
    border-radius: 10px;
    background: #74b3b6;
    color: white;

    :hover {
        background: blue;
        cursor:pointer;
    }
`

export const DivError = styled.div `
    display: flex;
    flex-direction: column;
    width: 100%;
    align-items: center;
    height: 20px;
`
export const LabelError = styled.div `
    font-size: 16px;
    font-weight: bold;
    color: red;
`