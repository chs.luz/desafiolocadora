import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import { 
    Container,
    ContainerLogin,
    Label,
    Input,
    Button,
    DivError,
    LabelError
} from './styles';

function Login({ history }) {

    const [login, setLogin ] = useState('');
    const [senha, setSenha ] = useState('');
    const [msgErro, setMsgErro ] = useState('');

    const changeTextLogin = (e) => {
        setLogin(e.target.value);
        setMsgErro('');
    }

    const changeTextSenha = (e) => {
        setSenha(e.target.value);
        setMsgErro('')
    }

    const logar = () => {
        if(login === "" || senha === "") {
            setMsgErro("Preencha o campo login ou senha");
        }
        else {
            localStorage.setItem("userId",login);
            history.push("/")
        }
    }

    return (
        <Container>
            <ContainerLogin>
                <Label>Login</Label>
                <Input 
                    placeholder="Digite seu login"
                    value={login}
                    onChange={changeTextLogin}/>
                <Label>Senha</Label>
                <Input 
                    placeholder="digite sua senha" 
                    type="password"
                    value={senha}
                    onChange={changeTextSenha}/>
                <Button onClick={logar}>Entrar</Button>
                <DivError>
                    <LabelError>{msgErro}</LabelError>
                </DivError>
            </ContainerLogin>
        </Container>
    )
}

export default withRouter(Login);