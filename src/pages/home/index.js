import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import Template from '../../components/Template';
import { 
    Container, 
    ContainerSearch, 
    ContainerContent, 
    InputSearch, 
    ButtonSearch, 
    ContainerNoContent,
    ContainerErro,
    LabelErro
 } from './styles';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { searchedFilm } from '../../store/actions/films';
import api, { findTitle } from '../../services/api';
import cinema from '../../assets/cinema.png';

import CardFilm from '../../components/card-film';

function Home(props) {

    const [title, setTitle] = useState('');
    const [msgErro, setMsgErro] = useState('');

    const searchFilm = async () => {
        if(title === '') {
            setMsgErro('Digite o nome do filme para pesquisar');
        }
        else {
            let response = await api.get(`${findTitle}${title}`);
            if(response.data.Error) {
                setMsgErro(response.data.Error)
                props.searchedFilm([]);
            }
            else {
                props.searchedFilm(response.data);
            }
        }
    }

    const change = (e) => {
        setTitle(e.target.value);
        setMsgErro('');
    }

    return (
        <Template {...props}>
            <Container>
                <ContainerSearch>
                    <InputSearch
                        value={title}
                        onChange={change}
                        placeholder="Digite o nome do filme"
                    />
                    <ButtonSearch 
                        onClick={searchFilm}>BUSCAR</ButtonSearch>
                </ContainerSearch>
                {msgErro !== '' && 
                    <ContainerErro>
                        <LabelErro>{msgErro}</LabelErro>
                    </ContainerErro>
                }
                <ContainerContent>
                    {
                        props.film.Title === undefined ?
                            <ContainerNoContent src={cinema} alt="">
                               
                               
                            </ContainerNoContent>
                            :
                            <CardFilm showDetails={false} film={props.film} {...props} />
                    }
                </ContainerContent>
            </Container>
        </Template>
    )
}

const mapStateToProps = state => {
    return {
        film: state.films.searchedFilm
    };
};

const mapDispatchToProps = dispath => {
    return bindActionCreators({ searchedFilm }, dispath);
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Home));