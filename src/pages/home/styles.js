import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    flex: 1;
`
export const ContainerErro = styled.div `
    height: 30px;
    width: 100%;
    display:flex;
    flex-direction: column;
    align-items:center;
`

export const LabelErro = styled.label `
    text-align:center;
    font-size:16px;
    color: red;
`

export const ContainerSearch = styled.div`
    display: flex;
    flex-direction: row;
    width: 90%;
    height: 50px;
    margin: 10px 50px;
`

export const InputSearch = styled.input`
    flex: 8;
    margin: 5px;
    height: 40px;
    padding: 0px 10px;
    border-radius: 10px;
    font-size: 16px;
    font-weight: bold;
`

export const ButtonSearch = styled.button`
    flex: 2;
    margin: 5px;
    height: 40px;
    border-radius: 10px;
    background: #74b3b6;
    color: white;

    :hover {
        background: blue;
        cursor:pointer;
    }
`

export const ContainerContent = styled.div`
    flex: 1;
    display: flex;
    flex-direction:column;
    padding: 2px;
    align-items:center;
`

export const ContainerNoContent = styled.img `
    display:flex;
    flex-direction: column;
    align-items:center;
    justify-content:center;
    width: 60%;
`