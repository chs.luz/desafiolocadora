import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { searchedFilm } from '../../store/actions/films';
import Template from '../../components/Template';
import api, { findById } from '../../services/api';
import CardFilm from '../../components/card-film';
import { Container } from './styles';

function FilmDetail({ searchedFilm, history, match, film }) {

    useEffect(() => {
        getFilmById()
    },[])

    const getFilmById = async () => {
        let imdbId = match.params.filmId;
        const response = await api.get(`${findById}${imdbId}`);
        searchedFilm(response.data);
    }


    return (
        <Template>
            <Container>
                <CardFilm showDetails={true} film={film} />
            </Container>

        </Template>
    )
}

const mapStateToProps = state => {
    return {
        film: state.films.searchedFilm
    };
};

const mapDispatchToProps = dispath => {
    return bindActionCreators({ searchedFilm }, dispath);
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(FilmDetail));