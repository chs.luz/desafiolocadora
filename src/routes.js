import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Login from './pages/login';
import Home from "./pages/home";
import FilmDetail from './pages/film-detail';
import FavoriteMovies from './pages/favorite-movies';

export default function Routes () {
    
    const isAuthenticate = () => {
      const userId = localStorage.getItem("userId");
      return userId !== null && userId !== "";
    }
    
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/film-detail/:filmId" render={() => isAuthenticate() ? <FilmDetail /> :  <Login />} />
          <Route path="/favorite-movies" render={() => isAuthenticate() ? <FavoriteMovies /> :  <Login />} />
          <Route path="/" render={() => isAuthenticate() ? <Home /> : <Login/>} />
        </Switch>
      </BrowserRouter>
    );
  }
  