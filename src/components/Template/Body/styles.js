import styled from 'styled-components';


export const Container = styled.main `
    height: 100%;
    background: #f0f2f5;
    position: relative;
    min-height: 800px;
`