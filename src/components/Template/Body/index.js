import React from "react";
import { Container } from './styles';

const Body = props => {
  return <Container>{props.children}</Container>;
};

export default Body;
