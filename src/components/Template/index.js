import React from "react";
import Header from "./Header";
import Body from "./Body";
import Footer from "./Footer";
import "./index.css";

const Template = props => {
  return (
    <div id="main-template">
      <Header {...props} />
      <Body {...props} />
      <Footer />
    </div>
  );
};

export default Template;
