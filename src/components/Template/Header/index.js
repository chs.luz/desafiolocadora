import React from "react";
import { Container, Title, Menu, MenuItem,LinkAlterado } from './styles';
import {  withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { searchedFilm, clearData } from '../../../store/actions/films';

const Header = props => {
  
  const redirectTo = (props, to) => {
    props.searchedFilm({})
    props.history.push(to);
  }

  const logout = () => {
    localStorage.clear();
    props.clearData();
  }

  return (
    <Container>
        <Title>Cine Movies</Title>
        <Menu>
          <MenuItem onClick={() => redirectTo(props,'/')}>Home</MenuItem>
          <MenuItem onClick={() => redirectTo(props,"/favorite-movies")}>My Favorites</MenuItem>
          <LinkAlterado to="/" onClick={logout}>logout</LinkAlterado>
        </Menu>
    </Container>
  );
};

const mapStateToProps = state => {
  return {
      film: state.films.searchedFilm
  };
};

const mapDispatchToProps = dispath => {
  return bindActionCreators({ searchedFilm, clearData }, dispath);
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Header));
