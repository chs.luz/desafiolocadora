import styled from "styled-components";
import { Link } from 'react-router-dom';

export const Container = styled.header`
  height: 80px;
  width: 100%;
  background: #74b3b6;
  display: flex;
  position: relative;
  align-items: center;
`;

export const Menu = styled.div`
  display: flex;
  flex-direction: "row";
  flex: 2;
`;

export const MenuItem = styled.div`
  align-items: center;
  justify-content: center;
  padding: 10px;
  font-size: 18px;

  &:hover {
    color: #0000ff;
  }
`;

export const Title = styled.label`
  margin-left: 30px;
  font-size: 24px;
  font-weight: bold;
  flex: 1;
`;


export const LinkAlterado = styled(Link) `
  position: absolute;
  right: 20px;
  top: 30px;
  font-size: 18px;
`