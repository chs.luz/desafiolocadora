import React from 'react';
import {
    Container,
    Title,
    ImagemPoster,
    InfoContainer,
    Info,
    LabelTitle,
    LabelContent,
    ContainerDescription,
    TextArea,
    ButtonMoreInfo,
    TextAreaDetail,
    Li,
    Ul,
    ContainerRating,
    ContainerMyFavorites,
    Favorite,
    NoFavorite
} from './styles';
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { addNewFavorite, removeFavoriteMovie } from '../../store/actions/films';


function CardFilm({addNewFavorite, removeFavoriteMovie, myFavorites, history, film, showDetails }) {

    const handleMoreInfo = () => {
        history.push(`film-detail/${film.imdbID}`);
    }

    const favorite = () => {
        const exist = myFavorites.filter(f => f.imdbID === film.imdbID);
        return exist.length > 0;
    }
    
    return (
        <Container>
            <ContainerMyFavorites>
                {
                    favorite() ?
                    <NoFavorite 
                        onClick={() => removeFavoriteMovie(film)} 
                        size={30}/>
                    :
                    <Favorite
                        onClick={() => addNewFavorite(film)}
                        size={30} />
                }
            </ContainerMyFavorites>
            <div key={film.imdbID}>
                <Title>{film.Title}</Title>
                <InfoContainer>
                    <ImagemPoster src={film.Poster} alt={film.Title}></ImagemPoster>
                    <Info>
                        <ContainerDescription>
                            <LabelTitle>Genre:</LabelTitle>
                            <LabelContent>{film.Genre}</LabelContent>
                        </ContainerDescription>
                        <ContainerDescription>
                            <LabelTitle>Director:</LabelTitle>
                            <LabelContent>{film.Director}</LabelContent>
                        </ContainerDescription>
                        <ContainerDescription>
                            <LabelTitle>Actors:</LabelTitle>
                            <LabelContent>{film.Actors}</LabelContent>
                        </ContainerDescription>
                        {
                            showDetails &&
                            <ContainerDescription>
                                <LabelTitle>Released:</LabelTitle>
                                <LabelContent>{film.Released}</LabelContent>
                            </ContainerDescription>
                        }
                        {
                            showDetails &&
                            <ContainerDescription>
                                <LabelTitle>Runtime:</LabelTitle>
                                <LabelContent>{film.Runtime}</LabelContent>
                            </ContainerDescription>
                        }
                        {
                            showDetails &&
                            <ContainerDescription>
                                <LabelTitle>Country:</LabelTitle>
                                <LabelContent>{film.Country}</LabelContent>
                            </ContainerDescription>
                        }
                        {
                            showDetails &&
                            <ContainerDescription>
                                <LabelTitle>Language:</LabelTitle>
                                <LabelContent>{film.Language}</LabelContent>
                            </ContainerDescription>
                        }
                        {
                            showDetails &&
                            <ContainerDescription>
                                <LabelTitle>Type:</LabelTitle>
                                <LabelContent>{film.Type}</LabelContent>
                            </ContainerDescription>
                        }
                        {
                            showDetails &&
                            <ContainerDescription>
                                <LabelTitle>Production:</LabelTitle>
                                <LabelContent>{film.Production}</LabelContent>
                            </ContainerDescription>
                        }
                        {
                            showDetails &&
                            <ContainerDescription>
                                <LabelTitle>Writer:</LabelTitle>
                                <LabelContent>{film.Writer}</LabelContent>
                            </ContainerDescription>
                        }
                        {
                            showDetails &&
                            <ContainerDescription>
                                <LabelTitle>Rating:</LabelTitle>
                                <LabelContent>{film.imdbRating}</LabelContent>
                            </ContainerDescription>
                        }
                        {
                            showDetails &&
                            <ContainerDescription>
                                <LabelTitle>Year:</LabelTitle>
                                <LabelContent>{film.Year}</LabelContent>
                            </ContainerDescription>
                        }
                        {
                            showDetails &&
                            <ContainerDescription>
                                <LabelTitle>Website:</LabelTitle>
                                <LabelContent><a href={film.Website} >{film.Website}</a></LabelContent>
                            </ContainerDescription>
                        }
                        {
                            !showDetails &&
                            <>
                                <ContainerDescription>
                                    <LabelTitle>Plot:</LabelTitle>
                                </ContainerDescription>

                                <TextArea disabled rows={4} cols={40} value={film.Plot} />
                                {!showDetails && <ButtonMoreInfo onClick={handleMoreInfo}>More Info</ButtonMoreInfo>}
                            </>
                        }
                    </Info>
                </InfoContainer>
                {
                    showDetails &&
                    <>
                        <ContainerDescription>
                            <LabelTitle>Plot:</LabelTitle>
                        </ContainerDescription>
                        <TextAreaDetail disabled rows={4} cols={40} value={film.Plot} />
                    </>
                }
                {
                    showDetails &&
                    <ContainerRating>
                        <LabelTitle>Rating:</LabelTitle>
                        <Ul>
                            {
                                film.Ratings && film.Ratings.map((rating, index) => (
                                    <Li key={index}>{rating.Source} - {rating.Value}</Li>
                                ))
                            }
                        </Ul>
                    </ContainerRating>

                }
            </div>
        </Container>
    )
}

const mapStateToProps = state => {
    return {
        myFavorites: state.films.favoriteMovies
    };
};

const mapDispatchToProps = dispath => {
    return bindActionCreators({ addNewFavorite, removeFavoriteMovie }, dispath);
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(CardFilm));