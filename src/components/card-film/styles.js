import styled from 'styled-components';
import { MdStar, MdStarBorder } from 'react-icons/md';

export const Container = styled.div `
    height:96%;
    width: 80%;
    margin: 5px;
    display:flex;
    flex-direction: column;
    flex: 1;
    background-color: white;
    padding: 5px;
`

export const Title = styled.p `
 font-size: 24px;
 width:400px;
 font-weight:bold;
 padding: 10px;
`
export const InfoContainer = styled.div`
    display:flex;
    flex: 1;
`
export const ImagemPoster = styled.img `
   height: 400px;
   flex:2;
`
export const Info = styled.div `
    display: flex;
    flex-direction:column;
    flex: 8;
    padding-left: 10px;
`

export const ContainerDescription = styled.div `
    display: flex;
    min-height: 35px;
`
export const LabelTitle = styled.label `
    margin: 2px;
    font-size: 18px;
    height:22px;
    font-weight: bold;
`

export const LabelContent = styled.label `
    margin: 3px;
    height:22px;
    font-size: 16px;
`

export const TextArea = styled.textarea `
    padding: 10px;
    height: 98px;
    width: 100%;
    resize: none;
`

export const TextAreaDetail = styled.textarea `
    padding: 10px;
    height: 98px;
    width: 100%;
    resize: none;
`

export const ButtonMoreInfo = styled.button `
    background: #74b3b6;
    color: white;
    height: 40px;
    margin-top:10px;
    width: 50%;
    border-radius: 10px;
    :hover{
        background: blue;
        cursor:pointer;
    }
`
export const Ul = styled.ul `
    margin-left: 15px;
`
export const Li = styled.li `
    margin-left: 10px;
    font-size: 24px;
`

export const ContainerRating = styled.div `
    margin-top: 20px;
`

export const ContainerMyFavorites = styled.div `
    width: 100%;
    height: 30px;
    display:flex;
    flex-direction: column;
    align-items: flex-end;
    padding: 0px 10px;
`

export const Favorite = styled(MdStarBorder) `
    :hover {
        color: red;
        cursor:pointer;
    }
`

export const NoFavorite = styled(MdStar) `
    :hover {
        color: gray;
        cursor:pointer;
    }
`