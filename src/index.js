import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware,compose  } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './store/reducers';
import App from './App';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, {}, composeEnhancers(applyMiddleware(ReduxThunk)));

const appWithRedux = (
    <Provider store={store}>
        <App />
    </Provider>
)

ReactDOM.render(appWithRedux, document.getElementById('root'));